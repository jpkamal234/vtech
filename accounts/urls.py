from django.urls import path
from.import views

urlpatterns = [
    
    path('',views.home,name='home'),
    path('aboutus/',views.aboutus,name='aboutus'),
    
    ##############authsection##########
    path('login/',views.user_login,name='login'),
    path('register/',views.register,name='register'),
    path('user_list/',views.user_list,name='user_list'),
    path('contactus/',views.contactus,name='contactus'),
    path('forgot_password/',views.forgot_password,name="forgot_password"),
    path('send_otp/', views.send_otp, name='send_otp'),
    path('verify_otp/', views.verify_otp, name='verify_otp'),
    ##########close auth#########################
        
    
#######PRODUCTSEACTION######

    path('add/', views.add_product, name='add_product'),
    path('product_list', views.product_list, name='product_list'),
    path('product/<int:product_id>/', views.product_detail, name='product_detail'),
    path('add-to-cart/<int:product_id>/', views.add_to_cart, name='add_to_cart'),

    
######CLOSE PRODUCTSEACTION##########    
    

    
    
]